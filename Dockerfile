FROM debian:bookworm-slim

# install httpd runtime dependencies
# https://httpd.apache.org/docs/2.4/install.html#requirements
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		ca-certificates \
		libaprutil1-ldap \
		apache2 \
		libapache2-mod-auth-openidc \		
# https://github.com/docker-library/httpd/issues/209
		libldap-common \
	; \
	rm -rf /var/lib/apt/lists/*

# Change ownership of directories that Apache needs to write to
RUN mkdir -p /var/run/apache2 /var/lock/apache2 /var/log/apache2 /usr/local/apache2/conf \
  && chown -R www-data /var/run/apache2 /var/lock/apache2 /var/log/apache2 /var/www/html /etc/apache2 /usr/local/apache2/conf

# Enable necessary modules
RUN a2enmod allowmethods headers proxy proxy_http mpm_event

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
STOPSIGNAL SIGWINCH

# Add custom httpd configuration
COPY conf/* /usr/local/apache2/conf/
RUN echo "" > /etc/apache2/ports.conf \
    && echo "IncludeOptional /usr/local/apache2/conf/httpd.conf" >> /etc/apache2/apache2.conf \
	&& rm /etc/apache2/sites-enabled/000-default.conf

EXPOSE 80

# TODO modify securityContext of the aptly Helm Chart
# Run the service with a non admin user   
#USER www-data

ENTRYPOINT ["/usr/sbin/apache2ctl"]
CMD ["-D", "FOREGROUND"]
